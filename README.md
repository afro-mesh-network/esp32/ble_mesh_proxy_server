| Supported Targets | ESP32 |
| ----------------- | ----- |

# ble mesh proxy server demo
## Introduction  
This demo implements ble mesh node basic features.Based on this demo, node can be scaned and proved by provisioner, reply get/set message to provisioner.  

Demo steps:  
1. Build the ble mesh proxy server demo with sdkconfig.default  
2. register node and set oob info, load model to init ble mesh node 
```
esp32> bmreg
E (13562) ble_mesh_console: enter ble_mesh_register_cb

I (13562) ble_mesh_console: Bm:Reg,OK
esp32> bmoob -o 0 -x 0
I (19442) ble_mesh_console: OOB:Load,OK

esp32> bminit -m 0x0000
I (24762) ble_mesh_console: Bm:Init,OK

esp32> bmnbearer -b 0x3 -e 1
I (62542) ble_mesh_console: Node:EnBearer,OK
``` 
3. enable bearer, so that it can be scaned and provisioned by provisioner  
